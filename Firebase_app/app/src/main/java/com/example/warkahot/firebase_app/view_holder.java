package com.example.warkahot.firebase_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by warkahot on 06-Aug-16.
 */
public class view_holder extends RecyclerView.ViewHolder {

    TextView name;
    ImageView image;
    CardView outlet_card;
    String img_url;

    public view_holder(View itemView) {
        super(itemView);

        name = (TextView)itemView.findViewById(R.id.text_outlet);
        image = (ImageView)itemView.findViewById(R.id.image_outlet);
        outlet_card = (CardView)itemView.findViewById(R.id.card_outlet);

        outlet_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString("outlet",name.getText().toString());
                System.out.println("String transferrred = " + name.getText().toString());
                b.putString("img_url", img_url);

                System.out.println("String image transferrred = " + img_url);
                Intent i = new Intent(name.getContext(),menu.class);
                i.putExtras(b);
                name.getContext().startActivity(i);
            }
        });
    }
}
