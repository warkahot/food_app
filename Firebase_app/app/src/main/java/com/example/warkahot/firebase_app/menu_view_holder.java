package com.example.warkahot.firebase_app;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by warkahot on 20-Aug-16.
 */
public class menu_view_holder extends RecyclerView.ViewHolder {

    ImageView item_image,n_nv_image;
    TextView item_text;
    LinearLayout card_item;
    String id;
    String image_url;
    String parent,menu_item;

    public menu_view_holder(View itemView) {
        super(itemView);
        item_image = (ImageView)itemView.findViewById(R.id.image_item);
        item_text = (TextView)itemView.findViewById(R.id.text_item);
        card_item = (LinearLayout)itemView.findViewById(R.id.card_item);
        n_nv_image = (ImageView)itemView.findViewById(R.id.v_nv_image_menu);


        card_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString("menu_item", menu_item);
                b.putString("id",id);
                b.putString("image_url",image_url);
                b.putString("parent",parent);
               System.out.println("String transferrred = "+b.get("menu_item"));
                Intent i = new Intent(item_image.getContext(),calorie_page.class);
                i.putExtras(b);
                item_image.getContext().startActivity(i);
            }
        });
    }
}
