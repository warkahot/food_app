package com.example.warkahot.firebase_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.devspark.appmsg.AppMsg;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class MainActivity extends AppCompatActivity{

    TextView tv;
    Button b1,b2;
    RecyclerView rv;
    ProgressDialog pd;
   // Firebase fb = new Firebase("https://fir-project1-ac09f.firebaseio.com/");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl("https://amber-heat-156.firebaseio.com/food_outlet");

        rv = (RecyclerView)findViewById(R.id.recycler_view);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new GridLayoutManager(this, 2));
        pd = new ProgressDialog(this);
        pd.setMessage("Please Wait...");


        /*fb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String str = dataSnapshot.getValue(String.class);
                tv.setText(str);
            }


            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });*/

        if(isNetworkAvailable()) {
            pd.show();
            FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<my_list, view_holder>(my_list.class, R.layout.card_food_outlet, view_holder.class, ref) {
                @Override
                protected void populateViewHolder(view_holder viewHolder, my_list model, int position) {
                    pd.dismiss();
                    viewHolder.name.setText(model.getName());
                    viewHolder.img_url = model.getURL();
                    System.out.println("Image url passed = "+viewHolder.img_url);
                    Picasso.with(MainActivity.this).load(model.getURL()).resize(300, 300).into(viewHolder.image);

                }
            };
            rv.setAdapter(adapter);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (pd.isShowing()) {
                        AppMsg amk = AppMsg.makeText(MainActivity.this, "Error getting data", AppMsg.STYLE_ALERT);
                        amk.show();
                        pd.dismiss();
                    }

                }
            }, 20000);
        }

        else
        {
            AppMsg appMsg = AppMsg.makeText(this,"Please check your Internet Connection",AppMsg.STYLE_ALERT);
            appMsg.show();
            pd.dismiss();
        }




      /*  b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fb.setValue("Button 1");
            }

        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fb.setValue("Button 2");
            }
        });*/


    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    public void onBackPressed() {

        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to exit!")
                .setConfirmText("Yes!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        Intent intent = new Intent(MainActivity.this, splash_login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("Exit me", true);
                        startActivity(intent);
                        finish();
                    }
                }).show();

    }
}
