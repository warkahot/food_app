package com.example.warkahot.firebase_app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devspark.appmsg.AppMsg;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by warkahot on 12-Aug-16.
 */
public class splash_login extends AppCompatActivity  implements
        GoogleApiClient.OnConnectionFailedListener{
    ImageView main_img;
    TextView tag_line;
    ImageView button_login;
    private GoogleApiClient mGoogleApiClient;
    private final int RC_SIGN_IN = 1;
    Context c;
    Intent ix;
    ProgressDialog pd;

    public void onCreate(Bundle b)
    {
        ix = new Intent(splash_login.this,MainActivity.class);
        c = this;
        super.onCreate(b);
        setContentView(R.layout.splash_login);
        pd = new ProgressDialog(this);
        pd.setMessage("Please Wait...");

        if( getIntent()!=null && getIntent().getBooleanExtra("Exit me", false)
                ){
            Log.d("File ","getting intent");
            finish();
            return;
        }

        getFileUser();



        main_img = (ImageView)findViewById(R.id.main_img);
        tag_line = (TextView)findViewById(R.id.tag_line);
        button_login = (ImageView)findViewById(R.id.button_g_login);

        tag_line.setVisibility(View.INVISIBLE);
        button_login.setVisibility(View.INVISIBLE);

         GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        final Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1000);


        Animation tAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move_top);
        tAnimation.setDuration(2000);
        tAnimation.setRepeatCount(0);
        tAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        tAnimation.setFillAfter(true);
        tAnimation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {


                tag_line.startAnimation(fadeIn);
                button_login.startAnimation(fadeIn);
            }
        });

        main_img.startAnimation(tAnimation);

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tag_line.setVisibility(View.VISIBLE);
                button_login.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    pd.show();
                    signIn();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (pd.isShowing()) {
                                AppMsg amk = AppMsg.makeText(splash_login.this, "Error getting data", AppMsg.STYLE_ALERT);
                                amk.show();
                                pd.dismiss();
                            }

                        }
                    }, 20000);
                }
                else
                {
                    AppMsg appMsg = AppMsg.makeText(splash_login.this,"Please check your Internet Connection",AppMsg.STYLE_ALERT);
                    appMsg.show();
                    pd.dismiss();
                }
            }
        });




    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }
    // [END onActivityResult]

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Toast.makeText(c,acct.getDisplayName()+" has logged in",Toast.LENGTH_SHORT).show();
           saveFileUser();
            pd.dismiss();
            Intent inx = new Intent(splash_login.this,MainActivity.class);
            startActivity(inx);


        } else {
            // Signed out, show unauthenticated UI.

        }
    }
    // [END handleSignInResult]

    // [START signIn]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void saveFileUser()
    {
        try {
            FileOutputStream fos = openFileOutput("pu.txt", Context.MODE_PRIVATE);
            String a ="a";
            fos.write(a.getBytes());
            fos.close();
            Log.d("File ","File saved");

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void getFileUser()
    {
        File f1 = getBaseContext().getFileStreamPath("pu.txt");
        if(f1.exists())
        {
            Log.d("File ","Does not exists");
            Intent in1 = new Intent(splash_login.this,MainActivity.class);
            startActivity(in1);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
