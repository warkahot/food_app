package com.example.warkahot.firebase_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.devspark.appmsg.AppMsg;
import com.firebase.client.FirebaseError;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

/**
 * Created by warkahot on 21-Aug-16.
 */
public class calorie_page extends AppCompatActivity {

    RecyclerView rv;
    ImageView item_image,v_nv_image;
    TextView item_name,item_weight,calorie_value,walk_time,cyc_time,v_nv;
    String parent;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calories_layout);

        String menu_item = "",item_id = "",image_url="";
        pd = new ProgressDialog(this);
        pd.setMessage("Please Wait...");



        item_image = (ImageView)findViewById(R.id.item_image_calorie);
        item_name = (TextView)findViewById(R.id.calories_layout_name);
        item_weight = (TextView)findViewById(R.id.calories_layout_weight);
        calorie_value = (TextView)findViewById(R.id.calorie_val);
        walk_time = (TextView)findViewById(R.id.walk_time);
        cyc_time = (TextView)findViewById(R.id.cyc_time);
        v_nv = (TextView)findViewById(R.id.vornv);
        v_nv_image = (ImageView)findViewById(R.id.v_nv_image);


        Intent i = getIntent();
        System.out.println("String got acd = "+menu_item);
        menu_item = i.getExtras().getString("menu_item");
        item_id = i.getExtras().getString("id");
        image_url = i.getExtras().getString("image_url");
        parent = i.getExtras().getString("parent");

        DatabaseReference refx = FirebaseDatabase.getInstance().getReferenceFromUrl("https://amber-heat-156.firebaseio.com/item_details");
        Query queryRefx = refx.orderByKey().equalTo(item_id);

        item_name.setText(menu_item);

        if(isNetworkAvailable()) {
            pd.show();
            queryRefx.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    System.out.println("There are " + snapshot.getChildrenCount() + " item details");
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        item_details details = postSnapshot.getValue(item_details.class);
                        item_weight.setText(details.getWeight());
                        calorie_value.setText(details.getCal());
                        walk_time.setText(details.getWalk());
                        cyc_time.setText(details.getCyc());
                        if(details.gettype().equals("v"))
                        {
                            v_nv_image.setImageResource(R.drawable.veg_icon);
                            v_nv.setText("Veg");
                        }
                        else
                        {
                            v_nv_image.setImageResource(R.drawable.n_veg_icon);
                            v_nv.setText("Non Veg");
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getMessage());
                }


            });

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (pd.isShowing()) {
                        AppMsg amk = AppMsg.makeText(calorie_page.this, "Error getting data", AppMsg.STYLE_ALERT);
                        amk.show();
                        pd.dismiss();
                    }

                }
            }, 20000);
        }
        else
        {
            AppMsg appMsg = AppMsg.makeText(this,"Please check your Internet Connection",AppMsg.STYLE_ALERT);
            appMsg.show();
            pd.dismiss();
        }

        Picasso.with(calorie_page.this).load(image_url).into(item_image);


        System.out.println("String menu_itm got = " + menu_item);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl("https://amber-heat-156.firebaseio.com/menu");
        ref = ref.child(parent);
        Query queryRef = ref.orderByKey().startAt("item").endAt("item\uf8ff");


        setTitle(menu_item);


        rv = (RecyclerView) findViewById(R.id.related_rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));



        FirebaseRecyclerAdapter<my_list, menu_view_holder> adapter = new FirebaseRecyclerAdapter<my_list, menu_view_holder>(my_list.class, R.layout.card_menu, menu_view_holder.class, queryRef) {
            @Override
            protected void populateViewHolder(menu_view_holder viewHolder, my_list model, int position) {
                pd.dismiss();
                viewHolder.item_text.setText("");
                viewHolder.menu_item = model.getName();
                viewHolder.image_url = model.getURL();
                viewHolder.parent = parent;

                if(model.getType().equals("v"))
                    viewHolder.n_nv_image.setImageResource(R.drawable.veg_icon);
                else
                    viewHolder.n_nv_image.setImageResource(R.drawable.n_veg_icon);

                Picasso.with(calorie_page.this).load(model.getURL()).resize(300, 300).transform(new CircleTransform()).into(viewHolder.item_image);
                viewHolder.id = model.getId();

            }
        };

        rv.setAdapter(adapter);

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
