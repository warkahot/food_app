package com.example.warkahot.firebase_app;

/**
 * Created by warkahot on 06-Aug-16.
 */
public class my_list
{
    String name,URL,id,type;

    public my_list()
    {}


    public my_list(String name, String URL,String id,String type) {
        this.name = name;
        this.URL = URL;
        this.id = id;
        this.type = type;

    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getURL() {
        return URL;
    }
}
