package com.example.warkahot.firebase_app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.firebase.client.Firebase;

/**
 * Created by warkahot on 06-Aug-16.
 */
public class App_Activity extends Application {

    public void onCreate()
    {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
