package com.example.warkahot.firebase_app;

/**
 * Created by warkahot on 21-Aug-16.
 */
public class item_details  {

    String cal,cyc,walk,weight,type;

    public item_details() {
    }

    public String gettype() {
        return type;
    }


    public item_details(String cal, String cyc, String walk, String weight,String type) {
        this.cal = cal;
        this.cyc = cyc;
        this.walk = walk;
        this.weight = weight;
        this.type = type;


    }

    public String getCal() {
        return cal;
    }

    public String getCyc() {
        return cyc;
    }

    public String getWalk() {
        return walk;
    }

    public String getWeight() {
        return weight;
    }
}
