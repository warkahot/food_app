package com.example.warkahot.firebase_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.devspark.appmsg.AppMsg;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

/**
 * Created by warkahot on 20-Aug-16.
 */
public class menu extends AppCompatActivity {


    RecyclerView rv;
    ImageView outlet_image;
    String menu_for = "",img_url="";
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prallax_menu_layout);

        outlet_image = (ImageView)findViewById(R.id.outlet_image);
        pd = new ProgressDialog(this);
        pd.setMessage("Please Wait...");

        Intent i = getIntent();
        System.out.println("String got acd = "+menu_for);
        menu_for = i.getExtras().getString("outlet");

        img_url = i.getExtras().getString("img_url");
        System.out.println("String got img_url = " + img_url);

        Picasso.with(menu.this).load(img_url).into(outlet_image);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl("https://amber-heat-156.firebaseio.com/menu");
        ref = ref.child(menu_for);
        Query queryRef = ref.orderByKey().startAt("item").endAt("item\uf8ff");


        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(menu_for);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        rv = (RecyclerView) findViewById(R.id.recycler_menu);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new GridLayoutManager(this, 3));


        if(isNetworkAvailable()) {
            pd.show();
            FirebaseRecyclerAdapter<my_list, menu_view_holder> adapter = new FirebaseRecyclerAdapter<my_list, menu_view_holder>(my_list.class, R.layout.card_menu, menu_view_holder.class, queryRef) {
                @Override
                protected void populateViewHolder(menu_view_holder viewHolder, my_list model, int position) {
                    pd.dismiss();
                    viewHolder.item_text.setText(model.getName());
                    viewHolder.menu_item = model.getName();
                    viewHolder.image_url = model.getURL();
                    viewHolder.parent = menu_for;
                    if(model.getType().equals("v"))
                        viewHolder.n_nv_image.setImageResource(R.drawable.veg_icon);
                    else
                        viewHolder.n_nv_image.setImageResource(R.drawable.n_veg_icon);

                    Picasso.with(menu.this).load(model.getURL()).resize(300, 300).transform(new CircleTransform()).into(viewHolder.item_image);
                    viewHolder.id = model.getId();

                }

            };

            rv.setAdapter(adapter);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (pd.isShowing()) {
                        AppMsg amk = AppMsg.makeText(menu.this, "Error getting data", AppMsg.STYLE_ALERT);
                        amk.show();
                        pd.dismiss();
                    }

                }
            }, 20000);

        }

        else
        {
            AppMsg appMsg = AppMsg.makeText(this,"Please check your Internet Connection",AppMsg.STYLE_ALERT);
            appMsg.show();
            pd.dismiss();
        }

    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    }
